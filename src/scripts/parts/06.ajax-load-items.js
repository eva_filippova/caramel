// News loader
(function () {
	var $load_list = $('.js-load-items'),
		$load_item = $('.js-load-item'),
		init_items = $load_item.length,
		URL = $load_list.data('api'),
		pages = $load_list.data('pages'),
		$btn = $('#show_more_items'),
		count = 1;

	$btn.click(function (e) {
		var $t = $(this);

		e.preventDefault();

		count++;

		if (count <= pages) {

			$.ajax({
				url: URL + '?page=' + count + '',
				cache: false,
				success: function (data, textStatus, jqXHR) {

					if ( 4 === jqXHR.readyState && 200 === jqXHR.status ) {

						for (var i = 0; i < init_items; i++) {
							$(data).appendTo($load_list);
						}

					}

				}
			});

			if (count === pages) {
				$t.hide();
			}
		}
	});
})();