// Slider with videos on the main page
(function () {
	var slider = "#video-block-slider";
	var $progress = $('.v-progress');
	var $pathItem = $('.js-video-path-item');
	var speedTime = $(slider).data('speed');

	var mySwiper = new Swiper(slider, {
		direction: 'horizontal',
		autoplay: {
			delay: speedTime,
			disableOnInteraction: false,
		},
		effect: 'fade',
		on: {
			slideChangeTransitionStart: function () {
				var self = this;
				var el = self.el;
				var videos = $(el).find('video');

				for (var i = 0; i < videos.length; i++) {
					videos[i].pause();
				}
			},
			slideChangeTransitionEnd: function () {
				var self = this;
				var el = self.el;
				var activeIndex = self.activeIndex;
				var video = $(el).find('video')[activeIndex];

				if (video.paused) {
					video.play();
				}
			},
			slideChange: function () {
				var self = this;
				var slideIndex = self.activeIndex;
				var $video = $(self.slides[slideIndex]).find("video")[0];

				$pathItem.removeClass('video-pagination__item_active');
				$('.js-video-path-item:eq(' + mySwiper.activeIndex + ')').addClass('video-pagination__item_active');
			},
			init: function () {
				var self = this;
				var el = self.el;
				var activeIndex = self.activeIndex;
				var videos = $(el).find('video');
				var video = $(el).find('video')[activeIndex];

				for (var i = 0; i < videos.length; i++) {
					videos[i].pause();
				}

				if (video.paused) {
					video.play();
				}

				$progress.find('.v-progress__line').css({
					"animation-duration": speedTime + "ms"
				});

				$('.js-video-path-item:eq(0)').addClass('video-pagination__item_active');
			},
		},
	});

	$pathItem.click(function () {
		var $t = $(this),
			pathIndex = $t.index();

		mySwiper.slideTo(pathIndex);
		$pathItem.removeClass('video-pagination__item_active');
		$t.addClass('video-pagination__item_active');

		return false;
	});
})();

// Slider on the 17_apartment-popup page
(function() {
	var mySwiper = new Swiper ('#flat-slider', {
		direction: 'horizontal',
		loop: false,
		autoplay: true,
		speed: 1000,
		navigation: {
			nextEl: '.flat-slider__button_next',
			prevEl: '.flat-slider__button_prev',
			clickable: true
		},
		pagination: {
			el: '.flat-slider__pagination',
			type: 'bullets',
			clickable: true,
			bulletClass: 'flat-slider__pagination-item',
			bulletActiveClass: 'flat-slider__pagination-item_active'
		},
	});
})();