(function() {
	var $building_section = $(".building__section");
	var building_section_left = ".building__section_left";
	var building_section_center = ".building__section_center";
	var building_section_right = ".building__section_right";
	var floors = ".building__floor";
	var $floor_tooltip = $(".floor-tooltip");
	var $number = $floor_tooltip.find(".floor-tooltip__number");
	var $sale = $floor_tooltip.find(".floor-tooltip__apartment_sale");
	var $finished = $floor_tooltip.find(".floor-tooltip__apartment_finished");
	var $price = $floor_tooltip.find(".floor-tooltip__price");

	showFloorTooltip(building_section_left, floors);
	showFloorTooltip(building_section_center, floors);
	showFloorTooltip(building_section_right, floors);

	$(floors).each(function (index, el) {
		var href = $(el).data("href");

		$(el).click(function () {
			$(window)[0].location.href = href;
		});
	});

	$building_section.on('mousemove', function(e) {
		var posX = void 0 === e.offsetX ? e.layerX : e.offsetX;
		var posY = void 0 === e.offsetY ? e.layerY : e.offsetY;
		var n = e.pageX + $floor_tooltip.outerWidth() + 10;
		var window_width = $(window).width();

		if (n < window_width) {
			$floor_tooltip.removeClass("floor-tooltip_bottom").css({
				"top": (posY - 45) + "px",
				"left": posX + 20 + "px"
			});
		} else {
			$floor_tooltip.addClass("floor-tooltip_bottom").css({
				"top": (posY + 20) + "px",
				"left": posX - (n - window_width) + "px"
			});
		}
	});

	function showFloorTooltip(svgGroupClass, svgFloorClass) {
		$(svgGroupClass + " " + svgFloorClass).hover(function (e) {
			var $el = $(e.currentTarget);
			var floor = $el.data("floor");
			var sale = $el.data("sale");
			var finished = $el.data("finished");
			var price = $el.data("price");

			$floor_tooltip.addClass("floor-tooltip_active");

			$number.html(floor);
			$sale.html(sale);
			$finished.html(finished);
			$price.html(price);

		}, function () {
			$floor_tooltip.removeClass("floor-tooltip_active");
		});
	};
})();