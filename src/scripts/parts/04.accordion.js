// Accordion on docs page
(function() {
	var $title = $('.js-accordion-title');

	$title.click(function(e) {
		e.preventDefault();

		var $t = $(this);
		var $parent = $t.closest(".accordion__item");
		var $titles = $parent.siblings().find($title);
		var $content = $t.next();
		var $contents = $parent.siblings().find(".accordion__item-content");

		$t.toggleClass("accordion__item-title-wrap_open");
		$titles.removeClass('accordion__item-title-wrap_open');
		$content.slideToggle('fast');
		$contents.slideUp('fast');

	});
})();
