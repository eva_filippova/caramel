// Reset filters
(function() {
	var $reset = $('#filters-reset-button');

	$reset.click(function() {
		var $t = $(this),
			$form = $t.closest('form'),
			$checkInputs = $form.find('.js-input-checkbox'),
			$rangeInputs = $form.find('.js-input-range'),
			$decorCheckbox = $('#filters-checkbox-decor');

		$checkInputs.prop('checked', false);
		$decorCheckbox.prop('checked', true);


		$rangeInputs.each(function(index, el) {
			var slider = $(el).data("ionRangeSlider");

			slider.reset();
		});

		return false;

	});
})();