(function() {
	var $w = $(window);
	var $timeline = $("#construction_timeline");
	var $progress = $timeline.find(".construction-timeline-wrap__item-progress");

	$progress.each(function (index, el) {
		var progress_value = $(el).data("progress");
		var $progress_active = $(el).find(".construction-timeline-wrap__progress-active");
		var $progress_icon = $(el).find(".construction-timeline-wrap__progress-icon");
		var progress_icon_width = $progress_icon.width();

		if ($w.width() <= 1235) {
			$progress_active.height(progress_value + "%");
		} else {
			$progress_active.width(progress_value + "%");
		}

		if (progress_value > 0) {
			$(el).addClass("construction-timeline-wrap__item-progress_active");
		}
	})
})();