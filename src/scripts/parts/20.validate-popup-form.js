(function() {
	var $popups = $('.popup');
	var $popup_success = $("#popup_success");
	var callTimeData = $("#call_time").data("ionRangeSlider");
	var $callDayRadioInput = $(".js-call-day");

	$popup_success && $popups.each(function (index, el) {
		if ("popup_success" !== el.id) {
			var $form = $(el).find(".popup-std__form");
			var $btn = $(el).find(".popup-std__form-btn");

			$btn.click(function (event) {
				event.preventDefault();

				var jsonURL;
				var $input = $form.find(".form__input");
				var form_data = $form.serialize();

				$input.each(function (index, el) {
					var value = $(el).val();

					if ($(el).hasClass("required") && "" === value) {
						$(el).addClass("form__input_fail");
					} else {
						$(el).removeClass("form__input_fail");
					}

					if ($(el).hasClass("form__input_phone") && value.length < 18) {
						$(el).addClass("form__input_fail");
					}

					if ($(el).hasClass("form__input_email") && (value.indexOf("@") <= 0 || value.indexOf(".") <= 0)) {
						$(el).addClass("form__input_fail");
					}

				});

				if ($form.find(".form__input_fail").length === 0) {
					jsonURL = $form.attr("action");

					$.ajax({
						type: "GET",
						url: jsonURL,
						data: form_data,
						crossDomain: true,
						success: function (data, textStatus, jqXHR) {

							if ( 4 === jqXHR.readyState && 200 === jqXHR.status ) {

								$.fancybox.close();

								// Reset form fields
								$input.each(function (index, el) {
									$(el).val("");
								});

								// Reset call day radio
								$callDayRadioInput.each(function (index, el) {
									if ($(el).val() === "today") {
										$(el).prop("checked", true);
									}

									if ($(el).val() === "tomorrow") {
										$(el).prop("checked", false);
									}
								});

								// Reset call time range slider
								callTimeData.reset();

								$.fancybox.open($popup_success);

							}

						},
						error: function (XMLHttpRequest, textStatus, errorThrown) {
							// console.log(XMLHttpRequest, textStatus, errorThrown);
						}
					});
				}
			});
		} else {
			$btn = $(el).find(".popup-std__form-btn");

			$btn.click(function (event) {
				event.preventDefault();
				$.fancybox.close();
			});
		}
	});
})();