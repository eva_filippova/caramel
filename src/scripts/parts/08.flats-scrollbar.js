// Flats scrollbar
(function() {
	$('#flats-list').mCustomScrollbar({
		axis:"y",
		theme:"dark",
		setHeight: "70vh",
		scrollInertia: 200
	});
})();