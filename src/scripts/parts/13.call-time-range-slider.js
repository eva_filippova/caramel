(function() {
	var $call_time = $("#call_time");
	var call_time_settings = {
		type: "double",
		grid: false,
		hide_min_max: true,
		postfix: ":00",
		min_interval: 1,
		min: 8,
		from_min: 10,
		from: 10,
		max: 22,
		to_max: 20,
		to: 20
	};
	$call_time.ionRangeSlider(call_time_settings);
})();