/**
 * Mobile menu open
 */

(function($) {
	var $burger = $('#header-burger'),
		$header = $('#header'),
		$mobileMenu = $('#mobile-menu'),
		$body = $("body");

	$burger.click(function() {
		$body.toggleClass('menu-open');
		$header.toggleClass('menu-open');
		$mobileMenu.toggleClass('mobile-menu_open');
		$burger.toggleClass('header__burger_open');

		return false;
	});
})(jQuery);

(function() {
	var $jsSubMenuOpen = $(".js-sub-nav-open");

	if ($(window).width() < 1015) {
		$jsSubMenuOpen.click(function (e) {
			var $t = $(this);
			var $subMenu = $t.next(".mobile-sub-menu");

			e.preventDefault();

			$subMenu.toggleClass("mobile-sub-menu_active");
		});
	}

})();