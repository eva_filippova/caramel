(function() {
	var $column = $('.js-table-head-col');
	var row_class = ".js-table-body-row";
	var opts = {
		touch : false,
		autoFocus : false,
		autoStart : false,
		backFocus : false
	};

	// REDIRECT
	$(document).on("click", row_class, function (e) {
		var href = $(e.currentTarget).data("href");
		var popup_id = "#popup_order";

		if (href !== popup_id) {
			$(window)[0].location.href = href;
		} else {
			$.fancybox.open($(popup_id), opts);
		}

	});

	$column.each(function (index, el) {
		var sort_type = $(el).data("sorting");

		if (typeof(sort_type) !== "undefined") {

			$(el).on("click", function () {
				var $self = $(this);

				if ($self.hasClass("flex-table__head-col_top")) {
					removeClassTableCol($column);
					$self.addClass("flex-table__head-col_bottom");
					sortingBody($self, !1);
				} else {
					removeClassTableCol($column);
					$self.addClass("flex-table__head-col_top");
					sortingBody($self, !0);
				}

			});
		}
	});
	
	function removeClassTableCol(element) {
		element.removeClass("flex-table__head-col_bottom");
		element.removeClass("flex-table__head-col_top");
	}

	function sortingBody(element, dd) {
		var type = element.data("sorting");
		var $table = element.closest("#sorting-table");
		var $row = $table.find(row_class);

		$row.sort(function (a, b) {
			var $text = $("[data-sorting='" + type + "'] .js-table-sort-data");
			var c = $(a).find($text).html().trim().toLowerCase();
			var d = $(b).find($text).html().trim().toLowerCase();

			return +c && +d && (c = +c, d = +d), dd ? sortBottom(c, d) : sortTop(c, d);

		}).each(function(i, e) {
			$(e).css({
				"order": i
			});
		});
	};

	function sortBottom(a, b) {
		return a > b ? 1 : a < b ? -1 : 0;
	};

	function sortTop(a, b) {
		return a < b ? 1 : a > b ? -1 : 0;
	};
})();