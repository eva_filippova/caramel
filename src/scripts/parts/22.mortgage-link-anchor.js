(function () {
	// LINK ANCHOR
	(function() {
		var $link = $('.js-link-anchor');

		$link.click(function (e){
			e.preventDefault();
			var href = $(this).attr('href');

			$('html, body').animate({
				scrollTop: $(href).offset().top
			}, 1000);
		});
	})();
})();