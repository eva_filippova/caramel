!function(t) {
	var e = !1;
	if ("function" === typeof define && define.amd && (define(t),
		e = !0),
	"object" === typeof exports && (module.exports = t(),
		e = !0),
		!e) {
		var n = window.Cookies,
			i = window.Cookies = t();
		i.noConflict = function() {
			return window.Cookies = n,
				i
		}
	}
}(function() {
	function t() {
		for (var t = 0, e = {}; t < arguments.length; t++) {
			var n = arguments[t];
			for (var i in n)
				e[i] = n[i]
		}
		return e
	}
	function e(n) {
		function i(e, o, r) {
			var a;
			if ("undefined" !== typeof document) {
				if (arguments.length > 1) {
					if (r = t({
						path: "/"
					}, i.defaults, r),
					"number" === typeof r.expires) {
						var s = new Date;
						s.setMilliseconds(s.getMilliseconds() + 864e5 * r.expires),
							r.expires = s
					}
					try {
						a = JSON.stringify(o),
						/^[\{\[]/.test(a) && (o = a)
					} catch (t) {}
					return o = n.write ? n.write(o, e) : encodeURIComponent(String(o)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent),
						e = encodeURIComponent(String(e)),
						e = e.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent),
						e = e.replace(/[\(\)]/g, escape),
						document.cookie = [e, "=", o, r.expires ? "; expires=" + r.expires.toUTCString() : "", r.path ? "; path=" + r.path : "", r.domain ? "; domain=" + r.domain : "", r.secure ? "; secure" : ""].join("")
				}
				e || (a = {});
				for (var u = document.cookie ? document.cookie.split("; ") : [], c = /(%[0-9A-Z]{2})+/g, l = 0; l < u.length; l++) {
					var d = u[l].split("=")
						, h = d.slice(1).join("=");
					'"' === h.charAt(0) && (h = h.slice(1, -1));
					try {
						var f = d[0].replace(c, decodeURIComponent);
						if (h = n.read ? n.read(h, f) : n(h, f) || h.replace(c, decodeURIComponent),
							this.json)
							try {
								h = JSON.parse(h)
							} catch (t) {}
						if (e === f) {
							a = h;
							break
						}
						e || (a[f] = h)
					} catch (t) {}
				}
				return a
			}
		}
		return i.set = i,
			i.get = function(t) {
				return i.call(i, t)
			}
			,
			i.getJSON = function() {
				return i.apply({
					json: !0
				}, [].slice.call(arguments))
			}
			,
			i.defaults = {},
			i.remove = function(e, n) {
				i(e, "", t(n, {
					expires: -1
				}))
			}
			,
			i.withConverter = e,
			i
	}
	return e(function() {})
});

(function () {
	var $cookie_dialog = $("#cookie_dialog");
	var $cookie_dialog_btn = $("#cookie_dialog_btn");
	var cookie_str = "cookie_dialog";
	var get_cookie = Cookies.get(cookie_str);

	$cookie_dialog_btn.click(function (event) {
		event.preventDefault();

		$cookie_dialog.hide();

		Cookies.set(cookie_str, "ok", {
			expires: 1
		});

	});

	if ("ok" !== get_cookie) {
		$cookie_dialog.show();
	} else {
		$cookie_dialog.hide();
	}


})();