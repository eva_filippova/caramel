// Contacts tabs
(function() {
	var $tab = $('.js-contacts-card-tab'),
		$content = $('.js-contacts-card-content');

	$tab.click(function() {
		var $t = $(this),
			id = $t.data('id'),
			$currentContent = $('#contacts-card-content_' + id);

		$tab.removeClass('contacts__card-tabs-item_active');
		$t.addClass('contacts__card-tabs-item_active');

		$content.removeClass('contacts__card-content_active');
		$currentContent.addClass('contacts__card-content_active');

		return false;
	});
})();