(function() {
	var $range_sum = $("#mortgage_sum"),
		initialSum = $range_sum.data("from"),
		$range_percent = $("#mortgage_percent"),
		initialPercent = $range_percent.data("from"),
		$range_period = $("#mortgage_period"),
		initialPeriod = $range_period.data("from"),
		$resultWrap = $('#calculator-result'),
		newSum,
		newPercent,
		newPeriod;

	// First calculation of mortgage
	calcMortgage();

	// Options for sum range slider
	$range_sum.ionRangeSlider({
		type: "single",
		grid: false,
		hide_min_max: true,
		postfix: " ₽",
		onStart: function (data) {},
		onChange: function (data) {
			var $element = $(data.slider[0]);

			tooltipPosition($element);

			// Set new sum
			setSum(data.from, 1);

			// Calculation of mortgage
			calcMortgage();
		},
	});

	// Options for percent range slider
	$range_percent.ionRangeSlider({
		type: "single",
		grid: false,
		hide_min_max: true,
		postfix: " %",
		onStart: function (data) {},
		onChange: function (data) {
			var $element = $(data.slider[0]);

			tooltipPosition($element);

			// Set new percent
			setPercent(data.from, 1);

			// Calculation of mortgage
			calcMortgage();
		}
	});

	// Options for period range slider
	$range_period.ionRangeSlider({
		type: "single",
		grid: false,
		hide_min_max: true,
		postfix: " лет",
		onStart: function (data) {},
		onChange: function (data) {
			var $element = $(data.slider[0]);

			tooltipPosition($element);

			// Set new period
			setPeriod(data.from, 1);

			// Calculation of mortgage
			calcMortgage();
		}
	});

	if ($range_sum.prev().length > 0) {
		tooltipPosition($($range_sum.prev()));
	}

	if ($range_percent.prev().length > 0) {
		tooltipPosition($($range_percent.prev()));
	}

	if ($range_period.prev().length > 0) {
		tooltipPosition($($range_period.prev()));
	}

	function tooltipPosition(el) {
		var slider_x_start = el.offset().left;
		var slider_x_end = el.width() + slider_x_start;
		var $tooltip = el.find(".irs-single");
		var tooltip_x_start = $tooltip.offset().left;
		var tooltip_x_end = $tooltip.width() + tooltip_x_start;

		if (tooltip_x_start <= slider_x_start) {
			$tooltip.css({"left": 0});
		}

		if (tooltip_x_end >= slider_x_end) {
			$tooltip.css({
				"left": "calc(100% - " + $tooltip.width() + "px" + ")",
			});
		}
	};

	function setSum(a, b) {
		newSum = b === 1 ? a : initialSum = a;
	};

	function setPercent(a, b) {
		newPercent = b === 1 ? a : initialPercent = a;
	};

	function setPeriod(a, b) {
		newPeriod = b === 1 ? a : initialPeriod = a;
	};

	// Calculate result of mortgage
	function calcMortgage() {
		var sum = typeof newSum !== "undefined" ? newSum : initialSum;
		var percent = typeof newPercent !== "undefined" ? newPercent : initialPercent;
		var period = typeof newPeriod !== "undefined" ? newPeriod : initialPeriod;
		var result;

		// Calculation itself
		result = sum * (percent / 100 / 12 * Math.pow((1 + percent / 100 / 12), (period * 12))) / (Math.pow((1 + percent / 100 / 12), (period * 12)) - 1);

		// Divide price with spaces
		$resultWrap.text(result.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
	};
})();