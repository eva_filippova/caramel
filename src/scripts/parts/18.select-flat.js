(function() {
	var $floor_svg = $(".floor__svg");
	var $floor_svg_back = $(".floor__svg-back");
	var $labels = $(".floor-labels");
	var floor = ".floor__svg";
	var flat = ".floor__flat";
	var $flat_tooltip = $(".flat-tooltip");
	var $number = $flat_tooltip.find(".flat-tooltip__number");
	var $area = $flat_tooltip.find(".flat-tooltip__area");
	var $finished = $flat_tooltip.find(".flat-tooltip__finished");
	var $price = $flat_tooltip.find(".flat-tooltip__price");
	var $rooms = $flat_tooltip.find(".flat-tooltip__rooms");

	showFlatTooltip(floor, flat);

	$(flat).each(function (index, el) {
		var href = $(el).data("href");

		$(el).click(function () {
			$(window)[0].location.href = href;
		});
	});

	$floor_svg.on('mousemove', function(e) {
		var posX = void 0 === e.offsetX ? e.layerX : e.offsetX;
		var posY = void 0 === e.offsetY ? e.layerY : e.offsetY;
		var n = e.pageX + $flat_tooltip.outerWidth() + 10;
		var window_width = $(window).width();

		if (n < window_width) {
			$flat_tooltip.removeClass("flat-tooltip_bottom").css({
				"top": posY + "px",
				"left": posX + 30 + "px"
			});
		} else {
			$flat_tooltip.addClass("flat-tooltip_bottom").css({
				"top": posY + 30 + "px",
				"left": posX - (n - window_width) + "px"
			});
		}
	});

	function showFlatTooltip(svgClass, svgFlatClass) {
		$(svgClass + " " + svgFlatClass).hover(function (e) {
			var $el = $(e.currentTarget);
			var flat = $el.data("flat");
			var flat_id = $el.data("flat-id");
			var area = $el.data("area");
			var finished = $el.data("finished");
			var price = $el.data("price");
			var rooms = $el.data("rooms");
			var $flat_back = $floor_svg_back.find('.floor__flat-back[data-flat-id="' + flat_id + '"]');
			var $label = $labels.find('.floor-labels__item[data-flat-id="' + flat_id + '"]');

			$flat_tooltip.addClass("flat-tooltip_active");
			$flat_back.addClass("floor__flat-back_active");
			$label.addClass("floor-labels__item_active");

			$number.text(flat);
			$area.text(area);
			$finished.text(finished);
			$price.text(price);
			$rooms.text(rooms);

		}, function (e) {
			var $el = $(e.currentTarget);
			var flat_id = $el.data("flat-id");
			var $flat_back = $floor_svg_back.find('.floor__flat-back[data-flat-id="' + flat_id + '"]');
			var $label = $labels.find('.floor-labels__item[data-flat-id="' + flat_id + '"]');

			$flat_tooltip.removeClass("flat-tooltip_active");
			$flat_back.removeClass("floor__flat-back_active");
			$label.removeClass("floor-labels__item_active");
		});
	};
})();