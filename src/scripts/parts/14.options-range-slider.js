(function() {
	var $optionsSquare = $('#options-square');
	var $optionsCost = $('#options-cost');
	var $OptionsFloor = $('#options-floor');

	$optionsSquare.ionRangeSlider({
		type: "double",
		grid: false,
		postfix: " м<sup>2</sup>",
		hide_min_max: true,
		extra_classes: "irs-invert"
	});

	$optionsCost.ionRangeSlider({
		type: "double",
		grid: false,
		hide_min_max: true,
		postfix: " млн. ₽",
		extra_classes: "irs-invert"
	});

	$OptionsFloor.ionRangeSlider({
		type: "double",
		grid: false,
		hide_min_max: true,
		extra_classes: "irs-invert"
	});
})();