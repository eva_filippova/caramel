(function () {
	// Fancybox
	$(".js-popup-link").fancybox({
		touch : false,
		autoFocus : false,
		autoStart : false,
		backFocus : false,
		afterLoad: function (instance, current) {
			if (current.src === "#popup_gallery") {
				slider_gallery_thumbs.init();
				slider_gallery.init();
			}
			if (current.src === "#popup_construction") {
				slider_construction.init();
			}
		},
		afterClose: function (instance, current) {
			if (current.src === "#popup_gallery") {
				slider_gallery_thumbs.slideTo(0);
				slider_gallery.slideTo(0);
			}
			if (current.src === "#popup_construction") {
				slider_construction.slideTo(0);
			}
		}
	});

	// Fancybox Close
	$(".js-popup-close").click(function () {
		$.fancybox.close();
	});

	// Galery Slider
	var slider_construction = new Swiper ('#slider_construction', {
		direction: 'horizontal',
		init: false,
		spaceBetween: 20,
		navigation: {
			nextEl: '.popup-slider-wrap__arrow_next',
			prevEl: '.popup-slider-wrap__arrow_prev'
		}
	});

	// Galery Slider
	var slider_gallery = new Swiper ('#slider_gallery', {
		direction: 'horizontal',
		init: false,
		spaceBetween: 20,
		navigation: {
			nextEl: '.popup-slider-wrap__arrow_next',
			prevEl: '.popup-slider-wrap__arrow_prev'
		}
	});

	// Gallery thumbs slider
	var slider_gallery_thumbs = new Swiper ('#slider_gallery_thumbs', {
		direction: 'horizontal',
		init: false,
		spaceBetween: 20,
		slidesPerView: 6,
		touchRatio: 0.2,
		centeredSlides: true,
		slideToClickedSlide: true,
		breakpoints: {
			1000: {
				slidesPerView: 3,
				spaceBetween: 10
			},
			1180: {
				slidesPerView: 4,
				spaceBetween: 10
			}
		}
	});

	// Sync gallery slider and thumbs
	if ($("#slider_gallery").length > 0 && $("#slider_gallery_thumbs").length > 0) {
		slider_gallery.controller.control = slider_gallery_thumbs;
		slider_gallery_thumbs.controller.control = slider_gallery;
	}
})();