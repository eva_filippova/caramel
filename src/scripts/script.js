$(document).ready(function() {
	$('body').addClass('loaded');


	//= parts/01.mobile-menu.js
	//= parts/02.lazyload.js
	//= parts/03.sliders.js
	//= parts/04.accordion.js

	//= parts/06.ajax-load-items.js
	//= parts/07.contacts-tabs.js
	//= parts/08.flats-scrollbar.js
	//= parts/09.gallery-slider.js
	//= parts/10.construction-timeline.js
	//= parts/11.mortgage-calculator.js
	//= parts/12.sort-table.js
	//= parts/13.call-time-range-slider.js
	//= parts/14.options-range-slider.js
	//= parts/15.reset-filters.js
	//= parts/18.select-flat.js
	//= parts/19.select-floor.js
	//= parts/20.validate-popup-form.js
	//= parts/21.set-cookie.js
	//= parts/22.mortgage-link-anchor.js
});
